#!/usr/bin/env python3

import json
import os
import requests
import sys
import warnings

from lib.job import *
from lib.repo import *
from lib.utils import *
import plugins

warnings.filterwarnings("ignore")

def __loop_api(call, payload):
    page = 1
    payload = payload or {}
    data = dict({ 'page': page }, **payload)
    result = []
    while True:
        r = get_request(call, data)
        result += r.json()
        if not r.headers['X-Next-Page']:
            break
        page += 1
        data['page'] = page
    return result

def __print_links(links):
    print(links)

def __merger_create(project_id, args):
    if len(args) >=2:
        target_branches = args[1].split(',')
    else:
        target_branches = ['master']
    if len(args) >= 3:
        source_branch = args[2]
    else:
        source_branch = get_current_branch()
    links = ''
    for target in target_branches:
        payload={'source_branch': source_branch,
                'target_branch': target,
                'labels': plugins.LABELS.get(target),
                'title': f"Merge {source_branch} to {target}" }
        print(f"creating merge request {source_branch} -> {target}")
        r = post_request(
                f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests',
                data = payload)
        links = links + f"\n{r.json()['web_url']}"
        status(r)
    __print_links(links)

def __merger_update(project_id, args):
    """
    Updates a merge request.
    command:
        gitlab merger update 9 add_lables dev,prod
    This will add labels dev,prod to merge request 9
    """
    iids = args[1].split(',')
    data = {args[2]: args[3]}
    for iid in iids:
        r = put_request(
            f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{iid}',
            data = data)
        reply = r.json()
        status(r)

def __merger_reopen(project_id, args):
    __merger_close(project_id, args)

def __merger_close(project_id, args):
    if len(args) < 2:
        print("usage: gitlab.py merger reopen|close <merge_id>")
        sys.exit(0)
    iids = args[1:]
    data = {"state_event": args[0]}
    for iid in iids:
        r = put_request(
            f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{iid}',
            data = data)
        reply = r.json()
        status(r)

def __merger_merge(project_id, args):
    iids = args[1:]
    for iid in iids:
        r = put_request(
            f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{iid}/merge')
        reply = r.json()
        status(r)

def __merger_delete(project_id, args):
    iid = args[1]
    r = delete_request(
        f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{iid}')
    status(r)

def __get_thumbsups(project_id, merge_request):
    r2 = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{merge_request["iid"]}/award_emoji')

    thumbsups = 0
    awardees = ''
    for award in r2.json():
        if award['name'] == 'thumbsup':
            thumbsups = thumbsups + 1
            user = award['user']['name'].split()[0]
            awardees = awardees + user + f":{award['id']},"
    awardees = ','.join(awardees.split(',')[:-1])
    return thumbsups, awardees

def __merger_thumbsup(project_id, args):
    merge_request_iids = args[1:]
    for merge_request_iid in merge_request_iids:
        r = post_request(
            f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{merge_request_iid}/award_emoji',
            data = {"name": "thumbsup"})
        status(r)

def __merger_remove_thumbsup(project_id, args):
    merge_request_iid = args[1]
    award_id = args[2]
    r = delete_request(
        f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests/{merge_request_iid}/award_emoji/{award_id}')
    status(r)

def __merger_list(project_id, args):
    if len(args) >= 2:
        state = args[1]
    else:
        state = 'opened'
    merge_requests = __loop_api(
            f'{GITLAB_URL}/api/v4/projects/{project_id}/merge_requests',
            { 'state': state })

    plugins.__print_merger_header()

    for merge_request in merge_requests:
        thumbsups, awardees = __get_thumbsups(project_id, merge_request)
        thumbsup_str = f"{str(thumbsups)} ({awardees})"
        plugins.__print_merger_body(merge_request, thumbsup_str, thumbsups)

def merger(args):
    """
    merge_request create target_branches [source_branch]
    """
    project_id = get_project_id()
    globals()[f"__merger_{args[0]}"](project_id, args)

def groups(args):
    if args[0] == 'update':
        group_id = args[1]
        page = 1
        data = {'page': page}
        projects_dict = {}
        while True:
            r = get_request(
                    f'{GITLAB_URL}/api/v4/groups/{group_id}/projects/',
                    data=data)
            for project in r.json():
                ssh_url_to_repo = project['ssh_url_to_repo']
                projects_dict[ssh_url_to_repo] = {}
                projects_dict[ssh_url_to_repo]['id'] = project['id']
                projects_dict[ssh_url_to_repo]['name'] = project['name']
            if not r.headers['X-Next-Page']:
                break
            page = page + 1
            data['page'] = page
            projects = json.load(open(f'{HOME}/.config/gitlab/projects.json', encoding='utf-8'))
            projects.update(projects_dict)
            with open(f'{HOME}/.config/gitlab/projects.json', 'w') as flh:
                json.dump(projects, flh)

def refresh(args):
    page = 1
    data = {"membership": True, "page": page, "per_page": 100}
    projects_dict = {}
    while True:
        r = get_request(
                f'{GITLAB_URL}/api/v4/projects/',
                data=data)
        for project in r.json():
            ssh_url_to_repo = project['ssh_url_to_repo']
            projects_dict[ssh_url_to_repo] = {}
            projects_dict[ssh_url_to_repo]['id'] = project['id']
            projects_dict[ssh_url_to_repo]['name'] = project['name']
        if not r.headers['X-Next-Page']:
            break
        page = page + 1
        data['page'] = page
    with open(f'{HOME}/.config/gitlab/projects.json', 'w') as flh:
        json.dump(projects_dict, flh)

def init(args):
    os.makedirs(f'{HOME}/.config/gitlab', exist_ok=True)

def main():
    try:
        cmd = sys.argv[1]
        globals()[cmd](sys.argv[2:])
    except IndexError as err:
        print(err)
        pass

if __name__ == '__main__':
    main()
