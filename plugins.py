class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNYELLOW = '\033[93m'
    FAILRED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

status_codes = {
        200: {"text": "OK", "color": bcolors.OKGREEN},
        201: {"text": "Created", "color": bcolors.OKGREEN},
        204: {"text": "OK: No Content", "color": bcolors.OKGREEN},
        304: {"text": "Not Modified", "color": bcolors.FAILRED},
        400: {"text": "Bad Request", "color": bcolors.FAILRED},
        401: {"text": "Unauthorized", "color": bcolors.FAILRED},
        403: {"text": "Forbidden", "color": bcolors.FAILRED},
        404: {"text": "Not Found", "color": bcolors.FAILRED},
        405: {"text": "Method Not Allowed", "color": bcolors.FAILRED},
        409: {"text": "Conflict", "color": bcolors.FAILRED},
        412: {"text": "Request Denied", "color": bcolors.FAILRED},
        422: {"text": "Unprocessable", "color": bcolors.FAILRED},
        429: {"text": "Too Many Requests", "color": bcolors.FAILRED},
        500: {"text": "Server Error", "color": bcolors.FAILRED}
        }

def __print_jobs_header():
    print(f"{bcolors.OKBLUE}{bcolors.BOLD}", end='')
    print("{:12s}".format('id'), end='')
    print("{:12s}".format('status'), end='')
    print("{:30s}".format('name'), end='')
    print("{:12s}".format('user'), end='')
    print(f"{bcolors.ENDC}")

def __print_job_body(job):
    if job['status'] == 'success':
        print(f"{bcolors.OKGREEN}{bcolors.BOLD}", end='')
    if job['status'] == 'failed':
        print(f"{bcolors.FAILRED}{bcolors.BOLD}", end='')
    if job['status'] == 'running':
        print(f"{bcolors.WARNYELLOW}{bcolors.BOLD}", end='')
    print(f"{str(job['id']):12s}", end='')
    print(f"{job['status']:12s}", end='')
    print(f"{job['name']:30s}", end='')
    print(f"{job['user']['username']:12s}", end='')
    print(f"{bcolors.ENDC}")

def __print_merger_header():
    print(f"{bcolors.OKBLUE}{bcolors.BOLD}", end='')
    print("{:12s}".format('id'), end='')
    print("{:12s}".format('iid'), end='')
    print("{:15s}".format('source'), end='')
    print("{:12s}".format('target'), end='')
    print("{:8s}".format('state'), end='')
    print("{:20s}".format('author'), end='')
    print("{:26s}".format('created'), end='')
    print("{:26s}".format('updated'), end='')
    print("{:8s}".format('WIP'), end='')
    print("{:10s}".format('conflicts'), end='')
    print("{:21s}".format('labels'), end='')
    print("{:10s}".format('thumbsups'), end='')
    print(f"{bcolors.ENDC}")

def __print_merger_body(merge_request, thumbsup_str, thumbsups):
    if merge_request['state'] == 'opened':
        print(f"{bcolors.OKGREEN}{bcolors.BOLD}", end='')
    print(f"{str(merge_request['id']):12s}", end='')
    print(f"{str(merge_request['iid']):12s}", end='')
    print(f"{merge_request['source_branch']:15s}", end='')
    print(f"{merge_request['target_branch']:12s}", end='')
    print(f"{merge_request['state']:8s}", end='')
    print(f"{merge_request['author']['name']:20s}", end='')
    print(f"{merge_request['created_at']:26s}", end='')
    print(f"{merge_request['updated_at']:26s}", end='')
    print(f"{str(merge_request['work_in_progress']):8s}", end='')
    print(f"{str(merge_request['has_conflicts']):10s}", end='')
    print(f"{','.join(merge_request['labels'])[:20]:21s}", end='')
    print(f"{thumbsup_str:10s}", end='')
    print(f"{bcolors.ENDC}")

LABELS = {
    'master': 'crowdnoetic'
}
