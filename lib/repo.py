from lib.utils import *
import plugins

def __get_latest_pipeline(project_id, args):
    if args:
        ref = args[0]
        response = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/pipelines/latest?ref={ref}')
    else:
        response = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/pipelines/latest')
    return response

def __get_latest_jobs(project_id, pipeline_id):
    response = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/pipelines/{pipeline_id}/jobs')
    return response

def __repo_pipeline(project_id, args):
    response = __get_latest_pipeline(project_id, args)
    print(response.json())

def __repo_job(project_id, args):
    latest_pipeline_id = __get_latest_pipeline(project_id, args).json()['id']
    latest_jobs = __get_latest_jobs(project_id, latest_pipeline_id)
    plugins.__print_jobs_header()
    for job in latest_jobs.json():
        plugins.__print_job_body(job)

def __repo_jobs(project_id, args):
    if args:
        pipeline_id = args[0]
        response = __get_latest_jobs(project_id, pipeline_id)
    else:
        response = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/jobs')
    for job in response.json():
        print(job)

def __repo_pipelines(project_id, merge_request):
    r2 = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/pipelines')
    for pipeline in r2.json():
        print(pipeline)

def repo(args):
    """
    merge_request create target_branches [source_branch]
    """
    project_id = get_project_id()
    globals()[f"__repo_{args[0]}"](project_id, args[1:])
