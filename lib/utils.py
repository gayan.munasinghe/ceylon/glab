import json
import os
import re
import requests
import subprocess
import plugins

def status(reply):
    status_code = reply.status_code
    print(f"{plugins.status_codes[status_code]['color']}{plugins.bcolors.BOLD}", end='')
    print(f"{status_code} - {plugins.status_codes[status_code]['text']}", end='')
    print(f"{plugins.bcolors.ENDC}")

def run_command(cmd):
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    return out.decode('utf-8').strip('\n')

def get_gitlab_url():
    cmd = 'git remote -v | grep origin.*fetch'
    url_line = run_command(cmd)
    p = re.compile(r'.*(gitlab.*?)(:|/).*')
    m = p.match(url_line)
    gitlab_url = f"https://{m[1]}"
    return gitlab_url

def get_project_id():
    cmd = f"git remote -v  |  grep {GITLAB_URL.split('/')[-1]}  |  grep fetch  |  awk '{{print $2}}'"
    project_key = run_command(cmd)
    projects = json.load(open(f'{HOME}/.config/gitlab/projects.json', encoding='utf-8'))
    return projects[project_key]["id"]

def get_current_branch():
    cmd = "git rev-parse --abbrev-ref HEAD"
    current_branch = run_command(cmd)
    return current_branch

GITLAB_URL = get_gitlab_url()
HOME = os.environ.get('HOME')

with open(f'{HOME}/.config/gitlab/token') as flh:
    PRIVATE_TOKEN = flh.read().strip('\n')

HEADERS = { "PRIVATE-TOKEN": PRIVATE_TOKEN }

def get_request(call, data=None):
    return requests.get(call, headers = HEADERS, data = data, verify = False)

def post_request(call, data=None):
    return requests.post(call, headers = HEADERS, data = data, verify = False)

def put_request(call, data=None):
    return requests.put(call, headers = HEADERS, data = data, verify = False)

def delete_request(call, data=None):
    return requests.delete(call, headers = HEADERS, data = data, verify = False)
