from lib.utils import *

def __run_job(project_id, args):
    action = args[0]
    job_id = args[1]
    if action == 'trace':
        response = get_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/jobs/{job_id}/{action}')
        print(response.text)
    else:
        response = post_request(f'{GITLAB_URL}/api/v4/projects/{project_id}/jobs/{job_id}/{action}')
    status(response)

def job(args):
    """
    Details of a single job
    """
    project_id = get_project_id()
    __run_job(project_id, args)
